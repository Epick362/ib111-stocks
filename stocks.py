from Analysis import *
from Stock import *
from Broker import *

stocks = ['AAPL', 'TSLA', 'YHOO']
stock_analysis = []

for stock in stocks:
    stock_analysis.append(Analysis(Stock(stock)))

starting_cash = raw_input('How much would you like to invest?')
trade_days = raw_input('For how many days would you like to keep investing?')

broker = Broker(int(starting_cash), stock_analysis)
broker.trade(int(trade_days))

for analysis in stock_analysis:
    plt.plot(list(float(s['Close']) for s in analysis.stock.data), label=analysis.stock.name)
plt.title('Analysis')
plt.ylabel('US Dollars')
plt.xlabel('Days')
plt.legend()
plt.show()

plt.plot(list(float(s - int(starting_cash)) for s in broker.cash_history), label='Broker Cash')
plt.ylabel('US Dollars')
plt.xlabel('Days')
plt.legend()
plt.show()
