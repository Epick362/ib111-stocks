import csv

class Stock:
    """docstring for Stock"""
    def __init__(self, name):
        self.name = name
        self.loadData()

    def loadData(self):
        self.data = []
        iterator = csv.DictReader(open('data/' + self.name + '.csv'))
        for row in iterator:
            self.data.append(row)
