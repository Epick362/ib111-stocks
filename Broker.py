from Analysis import *
import matplotlib.pyplot as plt

class Broker:
    def __init__(self, cash, stocks):
        self.stocks = stocks
        self.cash = cash
        self.starting_cash = cash
        self.cash_history = []
        self.invested = None
        self.invested_amout = 0.0

    def trade(self, days):
        for day in xrange(days):
            self.cash_history.append(self.cash)
            self.calculateCash(day)
            print 'Cash status: ${:.2f}'.format(self.cash)

            if isinstance(self.invested, Analysis):
                maxChange = self.invested.getDayDifference(day)
            else:
                maxChange = 0.0
            candidate = None

            for stock in self.stocks:
                diff = stock.getDayDifference(day)
                if(diff > maxChange):
                    maxChange = diff
                    candidate = stock

            if isinstance(candidate, Analysis):
                self.invest(candidate, self.cash)

                print 'Day', day + 1, 'invested into', candidate.stock.name, 'with change {:.2f}'.format(maxChange)
            else:
                print 'Day', day + 1, 'no suitable action found, staying with', self.invested.stock.name

            print

        print 'Results'
        print 'Starting cash ${}'.format(self.starting_cash)
        print 'End cash ${:.2f}'.format(self.cash)
        if (self.starting_cash > self.cash):
            print 'You lost', self.starting_cash - self.cash, 'dollars'
        else:
            print 'You gained', self.cash - self.starting_cash, 'dollars'

    def calculateCash(self, day):
        if isinstance(self.invested, Analysis):
            change = self.invested.getDayDifferenceInPercent(day)

            self.cash *= change

            print self.invested.stock.name, 'changed by {:.2f}%'.format((change - 1) * 100)

    def invest(self, stock, amount):
        self.invested = stock
        self.invested_amout = amount
