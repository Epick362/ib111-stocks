import math

class Analysis:
    """docstring for Analysis"""
    def __init__(self, stock):
        self.stock = stock
        self.average = self.average()
        self.change = self.change()
        self.minValue = self.minValue()
        self.maxValue = self.maxValue()

    def getDayDifference(self, day):
        today = self.stock.data[day]['Open']
        yesterday = self.stock.data[day - 1]['Open']

        return float(today) - float(yesterday)

    def getDayDifferenceInPercent(self, day):
        today = self.stock.data[day]['Open']
        yesterday = self.stock.data[day - 1]['Open']

        return float(today) / float(yesterday)

    def report(self):
        print '\n############ ----------- ############'
        print 'Analyzing Stock ' + self.stock.name
        print 'Average value %.2f' % self.average
        print 'Change %s%%' % self.change
        print 'Min %.2f' % self.minValue
        print 'Max %.2f' % self.maxValue

    def minValue(self):
        minimum = min(float(s['Close']) for s in self.stock.data)
        return minimum

    def maxValue(self):
        maximum = max(float(s['Close']) for s in self.stock.data)
        return maximum

    def average(self):
        total = 0.0

        i = 0
        for row in self.stock.data:
            total += float(row['Close'])
            i += 1

        avg = total / i

        return avg

    def change(self):
        start = float(self.stock.data[0]['Close'])
        end = float(self.stock.data[-1]['Close'])

        change = math.ceil(end / start * 100)
        sign = '+' if start <= end else '-'

        return change
